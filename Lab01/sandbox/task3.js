const fs = require('fs');
const os = require('os');

const userName = os.userInfo().username;

fs.writeFile('task3.txt', `Hello, ${userName}!`, (err) => {
    if (err) {
       return;
    }
    console.log('success');
});
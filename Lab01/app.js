const yargs = require("yargs");
const user = require("./user");

yargs.command(
    "add",
    "Add a language",
    {
        title: {
            type: "string",
            demandOption: true,
            describe: "Language title",
        },
        level: {
            type: "string",
            demandOption: true,
            describe: "Language level",
        },
    },
    function (argv) {
        console.log(user.add(argv.title, argv.level));
    }
).command(
    "list",
    "List of languages",
    function (argv) {
        console.log(user.list());
    }
).command(
    "read",
    "Read a level",
    {
        title: {
            type: "string",
            demandOption: true,
            describe: "Language title",
        }
    },
    function (argv) {
        console.log(user.read(argv.title));
    }
).parse();
const fs = require("fs")

function list() {
    let userJson = fs.readFileSync("user.json", "utf-8");
    let user = JSON.parse(userJson);
    return user.languages;
}
function read(title){
    try {
        let userJson = fs.readFileSync("user.json", "utf-8");
        let user = JSON.parse(userJson);
        let languages = user.languages
        languages.forEach((element) =>
        {
            console.log(element.title)
            if (element.title === title)
            {console.log("1")
                return element.level}
            else return "no language"
        })

    } catch (error) {
        return "123";
    }

}
function add(title, level) {
    try {
        let userJson = fs.readFileSync("user.json", "utf-8");
        let user = JSON.parse(userJson);
        let languages = user.languages
        languages.forEach((element) =>
            {
            if (element.title !== title) {
                user.languages.push({title, level})
                }
        })
        fs.writeFileSync("user.json", JSON.stringify(user));
        return {title, level};
    } catch (error) {
        return error.message;
    }
}

module.exports = {list, add, read}
const express = require('express')
const hbs = require('hbs')
const feth = require("node-fetch")
const app = express()
const port = 3000

app.set('view engine', 'hbs')
app.get('/weather(/:city?)', async (req, res) => {
    let city = req.params.city;
    if (!city) {
        city = req.query.city;
    }
    if (!city) {
        city= "Zhytomyr"
    }
    const apiKey = 'b5018676b6c9e7d01aa7056fd2b9186d';
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`
    console.log(url)
    let response = await fetch(url);
    let weather = await response.json();
    console.log(weather)
    res.render('weather', {city,weather})
});
app.listen(port,()=>{
    console.log("listening on 3000");
});
